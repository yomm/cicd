/**
 * View Models used by Spring MVC REST controllers.
 */
package com.yomma.cicd.web.rest.vm;
